//
//  ImageViewController.m
//  PicPal
//
//  Created by Geppy Parziale on 11/24/14.
//  Copyright (c) 2014 iNVASIVECODE, Inc. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()
@property IBOutlet UIImageView *imageView;

@end

@implementation ImageViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    NSData *imageData = [NSData dataWithContentsOfURL:self.imageURL];
    UIImage *image = [UIImage imageWithData:imageData];
    self.imageView.image = image;

    UIBarButtonItem *rotateButton = [[UIBarButtonItem alloc] initWithTitle:@"Rotate" style:UIBarButtonItemStylePlain target:self action:@selector(rotateImage)];
    [self.navigationItem setRightBarButtonItem:rotateButton];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    self.imageView = nil;
}

- (void)rotateImage
{
    CGImageRef currentCGImage = [self.imageView.image CGImage];

    CGSize originalSize = self.imageView.image.size;
    CGSize rotatedSize = CGSizeMake(originalSize.height, originalSize.width);

    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 rotatedSize.width,
                                                 rotatedSize.height,
                                                 CGImageGetBitsPerComponent(currentCGImage),
                                                 CGImageGetBitsPerPixel(currentCGImage) * rotatedSize.width,
                                                 CGImageGetColorSpace(currentCGImage),
                                                 CGImageGetBitmapInfo(currentCGImage));

    CGContextTranslateCTM(context, rotatedSize.width, 0.0f);
    CGContextRotateCTM(context, M_PI_2);
    CGContextDrawImage(context, (CGRect){.origin = CGPointZero, .size = originalSize}, currentCGImage);

    CGImageRef newCGImage = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newCGImage];
    self.imageView.image = newImage;
}


@end
